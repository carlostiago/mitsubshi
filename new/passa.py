#!/usr/bin/python
# -*- coding: ISO-8859-7 -*-
import urllib2
import json
import sys
import os
import time

print "Start : %s" % time.ctime()

#este bloco pega a categoria

url_category = "https://api.mercadolibre.com/categories/MLB1744"
response_category = urllib2.urlopen(url_category)
json_category = json.load(response_category)
with open('categoriesML.json', 'wb') as fp:
  json.dump(json_category, fp)
#https://api.mercadolibre.com/sites/MLB/search?category=MLB27427

#escreve o cabeçalho
f = open('./anuncios.csv','w')
f.write("ID_CLASSIFIED;TITLE;PRICE;AVAILABLE_QUANTITY;SOLD_QUANTITY;BUYING_MODE;LISTING_TYPE_ID;STOP_TIME;CONDITION;PERMALINK;ACCEPTS_MERCADOPAGO;ADDRESS_LINE;ZIP_CODE;COUNTRY;STATE;CITY;LATITUDE;LONGITUDE;CONTACT;OTHER_INFO;AREA_CODE;PHONE;AREA_CODE2;PHONE2;EMAIL;WEBPAGE;TRANSMISSAO;KM;MARCA;MODELO;ANO;VERSAO\n")
f.close()
y = 0
while y < 57:
  dict_data = json_category['children_categories'][y]
  valor =  json_category['children_categories'][y]['total_items_in_this_category']
  #print 'a= '+str(a)
  codigo =  json_category['children_categories'][y]['id']
  name =  json_category['children_categories'][y]['name']
  api = "https://api.mercadolibre.com/sites/MLB/search?category="

  y+=1
  n = 0
  x = valor / 100
  round(x)
  x = int(x) + 1
  print time.ctime(), codigo, name, valor, x
  while n <= x:
    #https://api.mercadolibre.com/sites/MLB/search?category=MLB6039&limit=50&offset=0
    url_classified = api + codigo + '&limit=100&offset='+ str(n) #+ '&sort=relevance'
    response_classified = urllib2.urlopen(url_classified)
    json_data = json.load(response_classified)
    file = name.replace(" ","_")
    with open(file + str(n)+'.json', 'wb') as fp:
      json.dump(json_data, fp)
    arquivo = file + str(n)+'.json'
    total = json_data['paging']['total']
    p = total / 100
    p = round(p)
    p = int(p) + 1
    n+=1
    z = 0
    #print total, p, n
    interador = json_data['results']
    #while z <=100:
    for d in interador:
      dict_data_anuncio = json_data['results'][z]
      id_classified =  json_data['results'][z]['id']
      ##print 'id= '+ id_classified
      title = json_data['results'][z]['title']
      title = title.replace(";"," ")
      ##print 'title= '+ title
      price = json_data['results'][z]['price']
      ##print "price= "+str(price)
      available_quantity = json_data['results'][z]['available_quantity']
      ##print 'available_quantity= '+ str(available_quantity)
      sold_quantity = json_data['results'][z]['sold_quantity']
      ##print "sold_quantity= "+str(sold_quantity)
      buying_mode = json_data['results'][z]['buying_mode']
      ##print "buying_mode= "+str(buying_mode)
      listing_type_id = json_data['results'][z]['listing_type_id']
      ##print "listing_type_id= "+str(listing_type_id)
      stop_time = json_data['results'][z]['stop_time']
      ##print "stop_time= "+str(stop_time)
      condition = json_data['results'][z]['condition']
      ##print "condition= "+str(condition)
      permalink = json_data['results'][z]['permalink']
      ##print "permalink= "+str(permalink)
      accepts_mercadopago = json_data['results'][z]['accepts_mercadopago']
      ##print 'accepts_mercadopago= '+str(accepts_mercadopago)
      address_line = json_data['results'][z]['seller_address']['address_line']
      address_line = address_line.replace(";"," ")
      address_line = address_line
      ##print 'address_line = '+address_line
      zip_code = json_data['results'][z]['seller_address']['zip_code']
      ##print "zip_code= "+str(zip_code)
      country = json_data['results'][z]['seller_address']['country']['name']
      ##print "country= "+str(country)
      state = json_data['results'][z]['seller_address']['state']['name']
      state = state
      ##print "state= "+state
      city = json_data['results'][z]['seller_address']['city']['name']
      city = city
      ##print "city= "+city
      latitude = json_data['results'][z]['seller_address']['latitude']
      ##print "latitude= "+str(latitude)
      longitude = json_data['results'][z]['seller_address']['longitude']
      ##print "longitude= "+str(longitude)
      contact = json_data['results'][z]['seller_contact']['contact']
      ##print "contact= "+str(contact)
      other_info = json_data['results'][z]['seller_contact']['other_info']
      ##print "other_info= "+str(other_info)
      area_code = json_data['results'][z]['seller_contact']['area_code']
      ##print "area_code= "+str(area_code)
      phone = json_data['results'][z]['seller_contact']['phone']
      ##print "phone= "+str(phone)
      area_code2 = json_data['results'][z]['seller_contact']['area_code2']
      ##print "area_code2= "+str(area_code2)
      phone2 = json_data['results'][z]['seller_contact']['phone2']
      ###print "phone2= "+str(phone2)
      email = json_data['results'][z]['seller_contact']['email']
      ###print "email= "+str(email)
      webpage = json_data['results'][z]['seller_contact']['webpage']

      attributes = json_data['results'][z]['attributes']
      c = 0
      transmissao = ''
      km = ''
      modelo = ''
      marca = ''
      versao = ''
      ano = ''
      for b in attributes:
        t =  json_data['results'][z]['attributes'][c]['id'].find('TRANS')
        k =  json_data['results'][z]['attributes'][c]['id'].find('KMTS')
        m =  json_data['results'][z]['attributes'][c]['id'].find('MARC')
        mo = json_data['results'][z]['attributes'][c]['id'].find('MODL')
        a =  json_data['results'][z]['attributes'][c]['id'].find('YEAR')
        v =  json_data['results'][z]['attributes'][c]['id'].find('VERS')
        if t != -1:
          transmissao = transmissao+' '+transmissao
          transmissao = json_data['results'][z]['attributes'][c]['value_name']
          transmissao = transmissao
        if k != -1:
          km = km+' '+km
          km = json_data['results'][z]['attributes'][c]['value_name']
          km = km
        if m != -1:
          marca = marca+' '+marca
          marca = json_data['results'][z]['attributes'][c]['value_name']
          marca = marca
        if mo != -1:
          modelo = modelo+' '+modelo
          modelo = json_data['results'][z]['attributes'][c]['value_name']
          modelo = modelo
        if a != -1:
          ano = ano+' '+ano
          ano = json_data['results'][z]['attributes'][c]['value_name']
          ano = ano
        if v != -1:
          versao = versao+' '+versao
          versao = json_data['results'][z]['attributes'][c]['value_name']
          versao = versao
        c+=1

      save1 = str(id_classified) + ';' + title+ ';' + str(price) + ';' + str(available_quantity) + ';' + str(sold_quantity) + ';' + buying_mode  + ';' + str(listing_type_id) + ';'
      save2 = str(stop_time) +';' + condition+ ';' + permalink + ';'+ str(accepts_mercadopago) + ';' +address_line + ';' + str(zip_code) + ';' + country+ ';'
      save3 =  state+ ';' + city+ ';' + str(latitude) + ';' + str(longitude) + ';' + contact+ ';' + other_info+ ';' + str(area_code)
      save4 =  ';' + str(phone) + ';' + str(area_code2) + ';' + str(phone2) + ';' + email+ ';' + webpage+ ';' + transmissao+ ';' + str(km) + ';' + marca+ ';' + modelo+ ';' + str(ano) + ';' + versao+ '\n'
      savetotal = save1+ save2+ save3+ save4
      f = open('./anuncios.csv','a')
      f.write(savetotal.encode('u8'))
      f.close()
      z +=1
    os.remove(arquivo)
    time.sleep( 5 )
print "End : %s" % time.ctime()
