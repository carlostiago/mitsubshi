#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib2
import json
import sys
import os

#este bloco pega a categoria
url_category = "https://api.mercadolibre.com/categories/MLB1744"
response_category = urllib2.urlopen(url_category)
json_category = json.load(response_category)
with open('categoriesML.json', 'wb') as fp:
  json.dump(json_category, fp)
#https://api.mercadolibre.com/sites/MLB/search?category=MLB27427

#escreve o cabeçalho
f = open('./anuncios.csv','w')
f.write("ID_CLASSIFIED;TITLE;PRICE;AVAILABLE_QUANTITY;SOLD_QUANTITY;BUYING_MODE;LISTING_TYPE_ID;STOP_TIME;CONDITION;PERMALINK;ACCEPTS_MERCADOPAGO;ADDRESS_LINE;ZIP_CODE;COUNTRY;STATE;CITY;LATITUDE;LONGITUDE;CONTACT;OTHER_INFO;AREA_CODE;PHONE;AREA_CODE2;PHONE2;EMAIL;WEBPAGE\n")#;TRANSMISSAO;KM;MARCA;MODELO;ANO;VERSAO\n")
f.close()
y = 0
while y < 60:
  dict_data = json_category['children_categories'][y]
  valor =  json_category['children_categories'][y]['total_items_in_this_category']
  #print 'a= '+str(a)
  codigo =  json_category['children_categories'][y]['id']
  name =  json_category['children_categories'][y]['name']
  api = "https://api.mercadolibre.com/sites/MLB/search?category="
  x = valor / 50
  round(x)
  x = int(x)
  n = 0
  while n <= 100:
    #https://api.mercadolibre.com/sites/MLB/search?category=MLB6039&limit=50&offset=0
    url_classified = api + codigo + '&limit=100&offset=' + str(n) #+ '&sort=relevance'
    response_classified = urllib2.urlopen(url_classified)
    json_data = json.load(response_classified)
    file = name.replace(" ","_")
    with open(file + str(n)+'.json', 'wb') as fp:
      json.dump(json_data, fp)

    arquivo = file + str(n)+'.json'
    os.remove(arquivo)
    n+=1
    z = 0
    while z <n:
      dict_data_anuncio = json_data['results'][z]
      id_classified =  json_data['results'][z]['id']
      ##print 'id= '+ id_classified
      title = json_data['results'][z]['title']
      title = title.replace(";"," ")
      ##print 'title= '+ title
      price = json_data['results'][z]['price']
      ##print "price= "+str(price)
      available_quantity = json_data['results'][z]['available_quantity']
      ##print 'available_quantity= '+ str(available_quantity)
      sold_quantity = json_data['results'][z]['sold_quantity']
      ##print "sold_quantity= "+str(sold_quantity)
      buying_mode = json_data['results'][z]['buying_mode']
      ##print "buying_mode= "+str(buying_mode)
      listing_type_id = json_data['results'][z]['listing_type_id']
      ##print "listing_type_id= "+str(listing_type_id)
      stop_time = json_data['results'][z]['stop_time']
      ##print "stop_time= "+str(stop_time)
      condition = json_data['results'][z]['condition']
      ##print "condition= "+str(condition)
      permalink = json_data['results'][z]['permalink']
      ##print "permalink= "+str(permalink)
      accepts_mercadopago = json_data['results'][z]['accepts_mercadopago']
      ##print 'accepts_mercadopago= '+str(accepts_mercadopago)
      address_line = json_data['results'][z]['seller_address']['address_line']
      address_line = address_line.replace(";"," ")
      ##print 'address_line = '+address_line
      zip_code = json_data['results'][z]['seller_address']['zip_code']
      ##print "zip_code= "+str(zip_code)
      country = json_data['results'][z]['seller_address']['country']['name']
      ##print "country= "+str(country)
      state = json_data['results'][z]['seller_address']['state']['name']
      ##print "state= "+state
      city = json_data['results'][z]['seller_address']['city']['name']
      ##print "city= "+city
      latitude = json_data['results'][z]['seller_address']['latitude']
      ##print "latitude= "+str(latitude)
      longitude = json_data['results'][z]['seller_address']['longitude']
      ##print "longitude= "+str(longitude)
      contact = json_data['results'][z]['seller_contact']['contact']
      ##print "contact= "+str(contact)
      other_info = json_data['results'][z]['seller_contact']['other_info']
      ##print "other_info= "+str(other_info)
      area_code = json_data['results'][z]['seller_contact']['area_code']
      ##print "area_code= "+str(area_code)
      phone = json_data['results'][z]['seller_contact']['phone']
      ##print "phone= "+str(phone)
      area_code2 = json_data['results'][z]['seller_contact']['area_code2']
      ##print "area_code2= "+str(area_code2)
      phone2 = json_data['results'][z]['seller_contact']['phone2']
      ###print "phone2= "+str(phone2)
      email = json_data['results'][z]['seller_contact']['email']
      ###print "email= "+str(email)
      webpage = json_data['results'][z]['seller_contact']['webpage']
      ###print "webpage= "+str(webpage)

    #  transmissao = json_data['results'][z]['attributes']
    #  v={}
    #  for x in tr:
    #    v[x['value']]=x['value_name']

      #transmissao = json_data['results'][z]['attributes'][0]['value_name']

    #  ['value_name']
      ###print "transmissao= "+str(transmissao)
    ##  km = json_data['results'][z]['attributes'][1]['value_name']
      ###print "km= "+str(km)
    #  marca = json_data['results'][z]['attributes'][2]['value_name']
      #print "marca= "+str(marca)+'Z = '+str(z)
    #  modelo = json_data['results'][z]['attributes'][3]['value_name']
      ###print "modelo= "+str(modelo)
    #  ano = json_data['results'][z]['attributes'][4]['value_name']
      #print "ano= "+str(ano)
      #versao = json_data['results'][z]['attributes'][5]['value_name']
      print z, url_classified
      ###print "versao= "+str(versao)
      save1 = str(id_classified) + ';' + title + ';' + str(price) + ';' + str(available_quantity) + ';' + str(sold_quantity) + ';' + buying_mode  + ';' + str(listing_type_id) + ';' + str(stop_time) + ';' + condition + ';' + permalink + ';'
      save2 = str(accepts_mercadopago) + ';' +address_line  + ';' + str(zip_code) + ';' + country + ';' + state + ';' + city + ';' + str(latitude) + ';' + str(longitude) + ';' + contact + ';' + other_info + ';' + str(area_code) + ';' + str(phone) + ';' + str(area_code2) + ';' + str(phone2) + ';' + email + ';' + webpage + '\n'
      # + ';' + transmissao + ';' + str(km) + ';' + marca + ';' + modelo + ';' + str(ano) + ';' + versao + '\n'
      savetotal = save1 + save2
      #print savetotal
      f = open('./anuncios.csv','a')
      f.write(savetotal.encode('u8'))
      f.close()
      z +=1

  #w = n -1
remove = file + str(w)+'.json'
print remove, url_classified
os.remove(remove)
y += 1
